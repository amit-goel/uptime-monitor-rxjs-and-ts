let formatDate = (timestamp: number) =>
    new Date(timestamp).toLocaleString('en-US', { timeZone: 'America/New_York' });

export = formatDate