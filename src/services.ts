import {Ping} from "./lib/ping";

let sites: Ping[] = [
    {
        website: 'https://test-site-awesomenew.herokuapp.com',
        timeout: 5,
        siteDown: false,
        notificationSent: false,
        name: 'Development Portal'
    },
    {
        website: 'https://portal.danabrainvital.com',
        timeout: 1,
        siteDown: false,
        notificationSent: false,
        name: 'Production Portal'
    }
];

export = sites;