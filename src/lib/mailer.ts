const nodemailer = require('nodemailer');
      import {provider}  from '../config/config';

let mailer = function (opts, fn) {

    let mailOpts, smtpTrans;

    // nodemailer configuration
    try {
        smtpTrans = nodemailer.createTransport(provider);
    } catch (err) {
        fn('Nodemailer could not create Transport', '');
        return;
    }

    // mailing options
    mailOpts = {
        from: opts.from,
        replyTo: opts.from,
        to: opts.to,
        subject: opts.subject,
        html: opts.body
    };

    // Send mail
    try {
        smtpTrans.sendMail(mailOpts, function (error, response) {
            error ? fn(true, error) : fn(false, response.message);
        });
    } catch (err) {
        fn('Nodemailer could not send Mail', '');
    }
};

export = mailer;