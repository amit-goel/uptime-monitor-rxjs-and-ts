export interface Ping {
    website: string;
    timeout: number;
    handle?: number;
    siteDown: boolean;
    notificationSent: boolean,
    timeStampDown?: number,
    name: string
}
