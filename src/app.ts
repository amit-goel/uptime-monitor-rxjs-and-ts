import {Ping} from './lib/ping';
import * as request from "request";
import {interval, Observable, of} from "rxjs";
import {flatMap, map, startWith} from "rxjs/internal/operators";
import sites from './services';
import mailer from './lib/mailer';
import formatDate from './util/utils';
import {Response} from "request";
import {FROM_FIELD, intervalInMs, TO_FIELD} from './config/config'

let sendMail = (site: Ping, sendTime: boolean) => {
    let htmlMsg = sendTime ? `<p>Time: ${formatDate(site.timeStampDown)} </p>` : '';
    htmlMsg += `<p>Website: ${site.website} </p>`;
    mailer(
        {
            from: FROM_FIELD,
            to: TO_FIELD,
            subject: `${site.name}  is ${site.siteDown ? 'down' : 'up'}`,
            body: htmlMsg
        },
        (error, res) => {
            if (error) {
                console.log(error);
            } else {
                site.notificationSent = !site.notificationSent;
                let msg = `${site.website}  ${site.siteDown ? 'down' : 'up'}.  Email is sending. `;
                console.log(msg + (sendTime ? `timestamp: ${site.timeStampDown}` : ""));
            }
        });
};

let processResponse = (site: Ping, response: Response) => {
    if (response.statusCode != 200) {
        if (!site.notificationSent) {
            site.timeStampDown = Date.now();
            site.siteDown = true;
            sendMail(site, true);
        }
    } else {
        if (site.siteDown) {
            site.siteDown = false;
            sendMail(site, false);
            site.timeStampDown = Date.now();
        }
    }

    return {...site, response: response};
};

let fetchContent = site => {
    return Observable.create(observer => {
        request.get(site.website, (error, response: Response, body) => {
            error ? observer.error(error.toString()) : observer.next(processResponse(site, response));
            observer.complete();
        });
    });
};

let flatten = arr => arr.reduce((flatArr, subArray) => flatArr.concat(subArray), []);
interval(intervalInMs)
    .pipe(
        startWith(0),
        map(() => sites),
        flatMap(items => of(...items)),
        flatMap(site => fetchContent(site))
    )
    .subscribe(
        (it: {website: String, response: Response}) =>
            console.log(`Site: ${it.website} Code: ${it.response.statusCode}`),
        e => console.log("error: " + e)
    );