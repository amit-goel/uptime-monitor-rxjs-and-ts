const pollingInterval = .5; // set Polling Interval in minutes

export const FROM_FIELD = 'FROM_EMAIL';
export const TO_FIELD = 'TO_EMAIL';

export const intervalInMs = pollingInterval * 60 * 1000;

export const provider = {
    service: 'gmail',
    /*port: 587,
    ignoreTLS: true,
    secure: false,*/
    auth: {
        user: 'USERNAME',
        pass: 'PASSWORD'
    }
};

