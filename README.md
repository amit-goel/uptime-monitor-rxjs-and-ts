# Uptime Monitoring for Wesbites

## Getting Started

Clone the project and add in specific email configuration and polling values in the config.ts file

Build project and compile TS to js file and deploy the generated dist folder to a service such as heroku

### Technical features

* **RxJS**

* **TypeScript**

* **NodeMailer**

## Notes

## Authors

* **Amit Goel** - *Initial work*

## License

This project is licensed under the MIT License